#!/bin/sh

# Stores the Widget service’s Spring Profiles in Consul’s hierarchical key/value store

set -e

CONSUL_SERVER=$(aws ec2 describe-instances \
  --filters Name='tag:Name,Values=tf-manager1' \
  --output text --query 'Reservations[*].Instances[*].PublicIpAddress')
echo "tf-manager1 public ip: ${CONSUL_SERVER}"

# default profile
KEY="config/widget-service/data"
VALUE="consul-configs/default.yml"
curl -X PUT --data-binary @${VALUE} \
  -H "Content-type: text/x-yaml" \
  ${CONSUL_SERVER:-localhost}:8500/v1/kv/${KEY}

# docker-local profile
KEY="config/widget-service,docker-local/data"
VALUE="consul-configs/docker-local.yml"
curl -X PUT --data-binary @${VALUE} \
  -H "Content-type: text/x-yaml" \
  ${CONSUL_SERVER:-localhost}:8500/v1/kv/${KEY}

# docker-production profile
KEY="config/widget-service,docker-production/data"
VALUE="consul-configs/docker-production.yml"
curl -X PUT --data-binary @${VALUE} \
  -H "Content-type: text/x-yaml" \
  ${CONSUL_SERVER:-localhost}:8500/v1/kv/${KEY}

echo "Script completed..."
