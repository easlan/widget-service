#!/bin/sh

# Creates external widget mongodb container data volumes and overlay network

set -e

vm="tf-manager1"

ec2_public_ip=$(aws ec2 describe-instances \
  --filters Name="tag:Name,Values=${vm}" \
  --output text --query 'Reservations[*].Instances[*].PublicIpAddress')

# create overlay network for stack
ssh -oStrictHostKeyChecking=no -T \
-i ~/.ssh/kartacatest.pem ubuntu@${ec2_public_ip} \
  docker network create \
    --driver overlay \
    --subnet=10.0.0.0/16 \
    --ip-range=10.0.11.0/24 \
    --opt encrypted \
    --attachable=true \
    widget_overlay_net
echo "Network completed..."

# create data volumes for MongoDB
vms=( "tf-manager1" "tf-manager2" "tf-manager3"
      "tf-worker1" "tf-worker2" "tf-worker3" "tf-worker4" )

for vm in "${vms[@]:3:4}"
do
  ec2_public_ip=$(aws ec2 describe-instances \
    --filters Name="tag:Name,Values=${vm}" \
    --output text --query 'Reservations[*].Instances[*].PublicIpAddress')
  ssh -oStrictHostKeyChecking=no -T \
    -i ~/.ssh/kartacatest.pem ubuntu@${ec2_public_ip} \
    docker volume create --name=widget_data_vol
  echo "Volume created: ${vm}..."
done

echo "Script completed..."
